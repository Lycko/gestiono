<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\CollaborateurRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;


/**
 * @Route("/user")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/", name="app_user_profil", methods={"GET"})
     */
    public function index(Request $request, UserRepository $userRepository, CollaborateurRepository $collab, PaginatorInterface $pagination): Response
    {
        $user = $this->getUser();
        $queryBuilder = $collab
        ->createQueryBuilder('c')
        ->select(CollaborateurRepository::ALIAS)
        ->leftJoin('c.ForfaitsCollabs', 'f')
        ->Where('c.Author = :id')
        ->setParameter('id', $user);

        $search = $request->query->get('search');
        if ($search) {
            $queryBuilder = $queryBuilder
                 ->where('c.nom like :nom ')
                 ->setParameter('nom', '%'.$search.'%')
                 ->orWhere('c.prenom like :prenom')
                 ->setParameter('prenom', '%'.$search.'%')
                 ->orwhere('c.entreprise like :entreprise ')
                 ->setParameter('entreprise', '%'.$search.'%')
                 ->andWhere('c.EmailUser = :id')
                 ->setParameter('id', $user);
        }

        $query = $queryBuilder->getQuery();

        $paginator = $pagination->paginate(
            $query,
            $request->query->getInt('page', 1), 3
        );

        return $this->render('user/index.html.twig', [
            'pagination' => $paginator,
            'user' => $userRepository->myProfil($user),
            'search' => $search,
        ]);
    }

    /**
     * @Route("/new", name="app_user_new", methods={"GET", "POST"})
     */
    public function new(Request $request, UserRepository $userRepository, UserPasswordEncoderInterface $userPasswordEncoderInterface): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setPassword(
                $userPasswordEncoderInterface->encodePassword($user, $user->getPassword())
            );
            
            $userRepository->add($user, true);

            return $this->redirectToRoute('app_user_profil', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('user/new.html.twig', [
            'user' => $user,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_user_show", methods={"GET"})
     */
    public function show(User $user): Response
    {
        return $this->render('user/show.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_user_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, User $user, UserRepository $userRepository): Response
    {
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $userRepository->add($user, true);

            return $this->redirectToRoute('app_user_profil', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('user/edit.html.twig', [
            'user' => $user,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_user_delete", methods={"POST"})
     */
    public function delete(Request $request, User $user, UserRepository $userRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $userRepository->remove($user, true);
        }

        return $this->redirectToRoute('app_user_profil', [], Response::HTTP_SEE_OTHER);
    }
}
