<?php

namespace App\Entity;

use App\Repository\FactureRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=FactureRepository::class)
 */
class Facture
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Assert\Type("\DateTimeInterface")
     */
    private $DateFacture;

    /**
     * @ORM\Column(type="boolean")
     */
    private $Etat;

    /**
     * @ORM\Column(type="integer")
     */
    private $NumFacture;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $FacturePDF;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $delaiePaiement;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $DateButoire;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="Factures")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Author;

    /**
     * @ORM\ManyToOne(targetEntity=Collaborateur::class, inversedBy="FacturesCollabs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $CollaborateursFact;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateFacture(): ?\DateTimeInterface
    {
        return $this->DateFacture;
    }

    public function setDateFacture(?\DateTimeInterface $DateFacture): self
    {
        $this->DateFacture = $DateFacture;

        return $this;
    }

    public function isEtat(): ?bool
    {
        return $this->Etat;
    }

    public function setEtat(bool $Etat): self
    {
        $this->Etat = $Etat;

        return $this;
    }

    public function getNumFacture(): ?int
    {
        return $this->NumFacture;
    }

    public function setNumFacture(int $NumFacture): self
    {
        $this->NumFacture = $NumFacture;

        return $this;
    }

    public function getFacturePDF(): ?string
    {
        return $this->FacturePDF;
    }

    public function setFacturePDF(?string $FacturePDF): self
    {
        $this->FacturePDF = $FacturePDF;

        return $this;
    }

    public function getDelaiePaiement(): ?int
    {
        return $this->delaiePaiement;
    }

    public function setDelaiePaiement(?int $delaiePaiement): self
    {
        $this->delaiePaiement = $delaiePaiement;

        return $this;
    }

    public function getDateButoire(): ?\DateTimeInterface
    {
        return $this->DateButoire;
    }

    public function setDateButoire(?\DateTimeInterface $DateButoire): self
    {
        $this->DateButoire = $DateButoire;

        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->Author;
    }

    public function setAuthor(?User $Author): self
    {
        $this->Author = $Author;

        return $this;
    }

    public function getCollaborateursFact(): ?Collaborateur
    {
        return $this->CollaborateursFact;
    }

    public function setCollaborateursFact(?Collaborateur $CollaborateursFact): self
    {
        $this->CollaborateursFact = $CollaborateursFact;

        return $this;
    }
}
