<?php

namespace App\Entity;

use App\Repository\ForfaitRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ForfaitRepository::class)
 */
class Forfait
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $NomFormule;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $NumFormule;

    /**
     * @ORM\Column(type="text")
     */
    private $Description;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $Quantite;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $MontantQ;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $NombreH;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $MontantH;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $TVA;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="Forfaits")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Author;

    /**
     * @ORM\ManyToMany(targetEntity=Collaborateur::class, mappedBy="ForfaitsCollabs")
     */
    private $CollaborateursForfaits;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $Remise;

    public function __construct()
    {
        $this->CollaborateursForfaits = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomFormule(): ?string
    {
        return $this->NomFormule;
    }

    public function setNomFormule(?string $NomFormule): self
    {
        $this->NomFormule = $NomFormule;

        return $this;
    }

    public function getNumFormule(): ?string
    {
        return $this->NumFormule;
    }

    public function setNumFormule(?string $NumFormule): self
    {
        $this->NumFormule = $NumFormule;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->Description;
    }

    public function setDescription(string $Description): self
    {
        $this->Description = $Description;

        return $this;
    }

    public function getQuantite(): ?int
    {
        return $this->Quantite;
    }

    public function setQuantite(?int $Quantite): self
    {
        $this->Quantite = $Quantite;

        return $this;
    }

    public function getMontantQ(): ?float
    {
        return $this->MontantQ;
    }

    public function setMontantQ(?float $MontantQ): self
    {
        $this->MontantQ = $MontantQ;

        return $this;
    }

    public function getNombreH(): ?float
    {
        return $this->NombreH;
    }

    public function setNombreH(?float $NombreH): self
    {
        $this->NombreH = $NombreH;

        return $this;
    }

    public function getMontantH(): ?float
    {
        return $this->MontantH;
    }

    public function setMontantH(?float $MontantH): self
    {
        $this->MontantH = $MontantH;

        return $this;
    }

    public function getTVA(): ?float
    {
        return $this->TVA;
    }

    public function setTVA(?float $TVA): self
    {
        $this->TVA = $TVA;

        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->Author;
    }

    public function setAuthor(?User $Author): self
    {
        $this->Author = $Author;

        return $this;
    }

    /**
     * @return Collection<int, Collaborateur>
     */
    public function getCollaborateursForfaits(): Collection
    {
        return $this->CollaborateursForfaits;
    }

    public function addCollaborateursForfait(Collaborateur $collaborateursForfait): self
    {
        if (!$this->CollaborateursForfaits->contains($collaborateursForfait)) {
            $this->CollaborateursForfaits[] = $collaborateursForfait;
            $collaborateursForfait->addForfaitsCollab($this);
        }

        return $this;
    }

    public function removeCollaborateursForfait(Collaborateur $collaborateursForfait): self
    {
        if ($this->CollaborateursForfaits->removeElement($collaborateursForfait)) {
            $collaborateursForfait->removeForfaitsCollab($this);
        }

        return $this;
    }

    public function getRemise(): ?float
    {
        return $this->Remise;
    }

    public function setRemise(?float $Remise): self
    {
        $this->Remise = $Remise;

        return $this;
    }
}
