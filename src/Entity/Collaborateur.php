<?php

namespace App\Entity;

use App\Repository\CollaborateurRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CollaborateurRepository::class)
 */
class Collaborateur
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Nom;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Prenom;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    private $Telephone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Adresse;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $CodePostal;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Ville;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $Reglement;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Entreprise;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="Collaborateurs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Author;

    /**
     * @ORM\ManyToMany(targetEntity=Forfait::class, inversedBy="CollaborateursForfaits")
     */
    private $ForfaitsCollabs;

    /**
     * @ORM\OneToMany(targetEntity=Facture::class, mappedBy="CollaborateursFact")
     */
    private $FacturesCollabs;

    public function __construct()
    {
        $this->ForfaitsCollabs = new ArrayCollection();
        $this->FacturesCollabs = new ArrayCollection();
    }

    public function GetCollaborateur(): self
    {
        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->Nom;
    }

    public function setNom(?string $Nom): self
    {
        $this->Nom = $Nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->Prenom;
    }

    public function setPrenom(?string $Prenom): self
    {
        $this->Prenom = $Prenom;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->Telephone;
    }

    public function setTelephone(?string $Telephone): self
    {
        $this->Telephone = $Telephone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->Email;
    }

    public function setEmail(?string $Email): self
    {
        $this->Email = $Email;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->Adresse;
    }

    public function setAdresse(?string $Adresse): self
    {
        $this->Adresse = $Adresse;

        return $this;
    }

    public function getCodePostal(): ?string
    {
        return $this->CodePostal;
    }

    public function setCodePostal(?string $CodePostal): self
    {
        $this->CodePostal = $CodePostal;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->Ville;
    }

    public function setVille(?string $Ville): self
    {
        $this->Ville = $Ville;

        return $this;
    }

    public function isReglement(): ?bool
    {
        return $this->Reglement;
    }

    public function setReglement(?bool $Reglement): self
    {
        $this->Reglement = $Reglement;

        return $this;
    }

    public function getEntreprise(): ?string
    {
        return $this->Entreprise;
    }

    public function setEntreprise(?string $Entreprise): self
    {
        $this->Entreprise = $Entreprise;

        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->Author;
    }

    public function setAuthor(?User $Author): self
    {
        $this->Author = $Author;

        return $this;
    }

    /**
     * @return Collection<int, Forfait>
     */
    public function getForfaitsCollabs(): Collection
    {
        return $this->ForfaitsCollabs;
    }

    public function addForfaitsCollab(Forfait $forfaitsCollab): self
    {
        if (!$this->ForfaitsCollabs->contains($forfaitsCollab)) {
            $this->ForfaitsCollabs[] = $forfaitsCollab;
        }

        return $this;
    }

    public function removeForfaitsCollab(Forfait $forfaitsCollab): self
    {
        $this->ForfaitsCollabs->removeElement($forfaitsCollab);

        return $this;
    }

    /**
     * @return Collection<int, Facture>
     */
    public function getFacturesCollabs(): Collection
    {
        return $this->FacturesCollabs;
    }

    public function addFacturesCollab(Facture $facturesCollab): self
    {
        if (!$this->FacturesCollabs->contains($facturesCollab)) {
            $this->FacturesCollabs[] = $facturesCollab;
            $facturesCollab->setCollaborateursFact($this);
        }

        return $this;
    }

    public function removeFacturesCollab(Facture $facturesCollab): self
    {
        if ($this->FacturesCollabs->removeElement($facturesCollab)) {
            // set the owning side to null (unless already changed)
            if ($facturesCollab->getCollaborateursFact() === $this) {
                $facturesCollab->setCollaborateursFact(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->Nom. ' '. $this->Prenom.' '.$this->Entreprise;
    }
}
