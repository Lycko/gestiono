<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType as TypeDateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType as TypeTextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('Nom', TypeTextType::class, [
                'label' => 'Nom', ])
            ->add('Prenom', TypeTextType::class, [
                'label' => 'Prénom', ])
            ->add('DateNaissance', TypeDateType::class, [
                'label' => 'Né(e) le',
                'attr' => ['placeholder' => 'dd/mm/yyyy'],
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'input' => 'datetime',
                'html5' => false,
                'required' => true,
                ])
            ->add('Telephone', TypeTextType::class, [
                'label' => 'Téléphone', ]) 
            ->add('SIRET', TypeTextType::class, [
                'label' => 'N° SIRET', ])
            ->add('Entreprise', TypeTextType::class, [
                'label' => 'Nom de l\'entreprise', ])
            ->add('SIREN', TypeTextType::class, [
                'label' => 'N° SIREN', ])
            ->add('AdresseUser', TypeTextType::class, [
                'label' => 'Adresse postale', ])
            ->add('AdresseEntreprise', TypeTextType::class, [
                'label' => 'Adresse de l\'entreprise', ])
            ->add('CodePostal', TypeTextType::class, [
                'label' => 'Code Postale', ])
            ->add('Ville', TypeTextType::class, [
                'label' => 'Ville', ])
            ->add('Pays', ChoiceType::class, [
                'choices'  => [
                    'France' => 'France',
                    'Belgique' => 'Belgique',
                    'Suisse' => 'Suisse',
                ],
            ])
            ->add('email', EmailType::class, [
            'label' => 'Adresse Mail', ])
            // ->add('agreeTerms', CheckboxType::class, [
            //     'mapped' => false,
            //     'constraints' => [
            //         new IsTrue([
            //             'message' => 'You should agree to our terms.',
            //         ]),
            //     ],
            // ])
            ->add('password', RepeatedType::class, array(
                'type' => PasswordType::class,
                'mapped' => false,
                'attr' => ['autocomplete' => 'new-password'],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter a password',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Your password should be at least {{ limit }} characters',
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                    ]),
                ],
                'first_options'  => array('label' => 'Mot de passe'),
                'second_options' => array('label' => 'Saisir à nouveau le mot de passe.'),
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
