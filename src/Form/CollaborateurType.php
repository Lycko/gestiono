<?php

namespace App\Form;

use App\Entity\Collaborateur;
use App\Entity\Forfait;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType as TypeTextType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security as CoreSecurity;

class CollaborateurType extends AbstractType
{
    private $security;

    public function __construct(CoreSecurity $security)
    {
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $user = $this->security->getUser();

        $builder
            ->add('Nom', TypeTextType::class, [
                'label' => 'Nom', ])
            ->add('Prenom', TypeTextType::class, [
                'label' => 'Prénom', ])
            ->add('Telephone', TypeTextType::class, [
                'label' => 'Téléphone', ])
            ->add('Email', EmailType::class, [
                'label' => 'Adresse mail', ])
            ->add('entreprise', TypeTextType::class, [
                'label' => 'Nom de l\'Entreprise', ])
            ->add('Adresse', TypeTextType::class, [
                'label' => 'Adresse postale', ])
            ->add('CodePostal', TypeTextType::class, [
                'label' => 'Code postale', ])
            ->add('Ville', TypeTextType::class, [
                'label' => 'Ville', ])
            ->add('Reglement', CheckboxType::class, [
                'label' => 'Réglement',
                'label_attr' => [
                    'class' => 'checkbox-switch',  
                ],
                'required' => false, ])
            ->add('ForfaitsCollabs', EntityType::class, [
                'class' => Forfait::class,
                'choice_label' => function (Forfait $forfait) {
                    return 'Formule N° '.$forfait->getNumFormule().' '.$forfait->getNomFormule();
                },
                'label' => 'Formules',
                'required' => false,
                'query_builder' => function (EntityRepository $er) use ($user) {
                    return $er->createQueryBuilder('c')
                        ->select('c')
                        ->innerJoin('c.Author', 'u')
                        ->where('c.Author = :user')
                        ->setParameter(':user', $user->getId())
                        ->orderBy('c.NomFormule', 'ASC')
                        ;
                },
                'multiple' => true,
                'expanded' => true,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Collaborateur::class,
        ]);
    }
}


/*
<div class="form-check form-switch">
  <input class="form-check-input" type="checkbox" role="switch" id="flexSwitchCheckChecked" checked />
  <label class="form-check-label" for="flexSwitchCheckChecked">Checked switch checkbox input</label>
</div>
*/
