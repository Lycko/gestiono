<?php

namespace App\Form;

use App\Entity\Collaborateur;
use App\Entity\Forfait;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType as TypeTextType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class ForfaitType extends AbstractType
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $user = $this->security->getUser();
        $builder
            ->add('NomFormule', TypeTextType::class, [
                'label' => 'Nom', ])
            ->add('NumFormule', NumberType::class, [
                'label' => 'N° Forfait', ])
            ->add('Description', TextareaType::class, 
            ['attr' => ['placeholder' => 'Renseigner une déscription. ', 'rows' => 8]])
            ->add('Quantite', NumberType::class, [
                'label' => 'Quantité',
                'required' => false ])
            ->add('MontantQ', NumberType::class, [
                'label' => 'Montant unique quantité',
                'required' => false ])
            ->add('NombreH', NumberType::class, [
                'label' => 'Nombre d\'heure pour la réalisation',
                'required' => false ])
            ->add('MontantH', NumberType::class, [
                'label' => 'Montant unique par heure.',
                'required' => false ])
            ->add('TVA', NumberType::class, [
                'label' => 'Montant de la TVA', 
                'required' => false,])
            ->add('Collaborateurs_Forfaits', EntityType::class, [
                'class' => Collaborateur::class,
                'choice_label' => function (Collaborateur $collaborateur) {
                    return $collaborateur->getNom().' '.$collaborateur->getPrenom().' '.$collaborateur->getEntreprise();
                },
                'label' => 'Collaborateurs',
                'multiple' => true,
                'required' => false,
                'by_reference' => false, // a ajouter en relation manyToMany quand une des 2 entité ne persist pas dans un NEW
                'query_builder' => function (EntityRepository $er) use ($user) {
                    return $er->createQueryBuilder('c')
                    ->select('c')
                    ->innerJoin('c.Author', 'u')
                    ->where('c.Author = :user')
                    ->setParameter(':user', $user->getId())
                    ->orderBy('c.Nom', 'ASC');
                },
                'expanded' => true, ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Forfait::class,
        ]);
    }
}
