<?php

namespace App\Test\Controller;

use App\Entity\Forfait;
use App\Repository\ForfaitRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ForfaitControllerTest extends WebTestCase
{
    private KernelBrowser $client;
    private ForfaitRepository $repository;
    private string $path = '/forfait/';

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->repository = (static::getContainer()->get('doctrine'))->getRepository(Forfait::class);

        foreach ($this->repository->findAll() as $object) {
            $this->repository->remove($object, true);
        }
    }

    public function testIndex(): void
    {
        $crawler = $this->client->request('GET', $this->path);

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Forfait index');

        // Use the $crawler to perform additional assertions e.g.
        // self::assertSame('Some text on the page', $crawler->filter('.p')->first());
    }

    public function testNew(): void
    {
        $originalNumObjectsInRepository = count($this->repository->findAll());

        $this->markTestIncomplete();
        $this->client->request('GET', sprintf('%snew', $this->path));

        self::assertResponseStatusCodeSame(200);

        $this->client->submitForm('Save', [
            'forfait[NomFormule]' => 'Testing',
            'forfait[NumFormule]' => 'Testing',
            'forfait[Description]' => 'Testing',
            'forfait[Quantite]' => 'Testing',
            'forfait[MontantQ]' => 'Testing',
            'forfait[NombreH]' => 'Testing',
            'forfait[MontantH]' => 'Testing',
            'forfait[TVA]' => 'Testing',
        ]);

        self::assertResponseRedirects('/forfait/');

        self::assertSame($originalNumObjectsInRepository + 1, count($this->repository->findAll()));
    }

    public function testShow(): void
    {
        $this->markTestIncomplete();
        $fixture = new Forfait();
        $fixture->setNomFormule('My Title');
        $fixture->setNumFormule('My Title');
        $fixture->setDescription('My Title');
        $fixture->setQuantite('My Title');
        $fixture->setMontantQ('My Title');
        $fixture->setNombreH('My Title');
        $fixture->setMontantH('My Title');
        $fixture->setTVA('My Title');

        $this->repository->add($fixture, true);

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Forfait');

        // Use assertions to check that the properties are properly displayed.
    }

    public function testEdit(): void
    {
        $this->markTestIncomplete();
        $fixture = new Forfait();
        $fixture->setNomFormule('My Title');
        $fixture->setNumFormule('My Title');
        $fixture->setDescription('My Title');
        $fixture->setQuantite('My Title');
        $fixture->setMontantQ('My Title');
        $fixture->setNombreH('My Title');
        $fixture->setMontantH('My Title');
        $fixture->setTVA('My Title');

        $this->repository->add($fixture, true);

        $this->client->request('GET', sprintf('%s%s/edit', $this->path, $fixture->getId()));

        $this->client->submitForm('Update', [
            'forfait[NomFormule]' => 'Something New',
            'forfait[NumFormule]' => 'Something New',
            'forfait[Description]' => 'Something New',
            'forfait[Quantite]' => 'Something New',
            'forfait[MontantQ]' => 'Something New',
            'forfait[NombreH]' => 'Something New',
            'forfait[MontantH]' => 'Something New',
            'forfait[TVA]' => 'Something New',
        ]);

        self::assertResponseRedirects('/forfait/');

        $fixture = $this->repository->findAll();

        self::assertSame('Something New', $fixture[0]->getNomFormule());
        self::assertSame('Something New', $fixture[0]->getNumFormule());
        self::assertSame('Something New', $fixture[0]->getDescription());
        self::assertSame('Something New', $fixture[0]->getQuantite());
        self::assertSame('Something New', $fixture[0]->getMontantQ());
        self::assertSame('Something New', $fixture[0]->getNombreH());
        self::assertSame('Something New', $fixture[0]->getMontantH());
        self::assertSame('Something New', $fixture[0]->getTVA());
    }

    public function testRemove(): void
    {
        $this->markTestIncomplete();

        $originalNumObjectsInRepository = count($this->repository->findAll());

        $fixture = new Forfait();
        $fixture->setNomFormule('My Title');
        $fixture->setNumFormule('My Title');
        $fixture->setDescription('My Title');
        $fixture->setQuantite('My Title');
        $fixture->setMontantQ('My Title');
        $fixture->setNombreH('My Title');
        $fixture->setMontantH('My Title');
        $fixture->setTVA('My Title');

        $this->repository->add($fixture, true);

        self::assertSame($originalNumObjectsInRepository + 1, count($this->repository->findAll()));

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));
        $this->client->submitForm('Delete');

        self::assertSame($originalNumObjectsInRepository, count($this->repository->findAll()));
        self::assertResponseRedirects('/forfait/');
    }
}
