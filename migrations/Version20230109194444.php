<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230109194444 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE collaborateur CHANGE user_id author_id INT NOT NULL');
        $this->addSql('ALTER TABLE collaborateur ADD CONSTRAINT FK_770CBCD3F675F31B FOREIGN KEY (author_id) REFERENCES `user` (id)');
        $this->addSql('CREATE INDEX IDX_770CBCD3F675F31B ON collaborateur (author_id)');
        $this->addSql('ALTER TABLE facture ADD author_id INT NOT NULL');
        $this->addSql('ALTER TABLE facture ADD CONSTRAINT FK_FE866410F675F31B FOREIGN KEY (author_id) REFERENCES `user` (id)');
        $this->addSql('CREATE INDEX IDX_FE866410F675F31B ON facture (author_id)');
        $this->addSql('ALTER TABLE forfait ADD author_id INT NOT NULL');
        $this->addSql('ALTER TABLE forfait ADD CONSTRAINT FK_BBB5C482F675F31B FOREIGN KEY (author_id) REFERENCES `user` (id)');
        $this->addSql('CREATE INDEX IDX_BBB5C482F675F31B ON forfait (author_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE collaborateur DROP FOREIGN KEY FK_770CBCD3F675F31B');
        $this->addSql('DROP INDEX IDX_770CBCD3F675F31B ON collaborateur');
        $this->addSql('ALTER TABLE collaborateur CHANGE author_id user_id INT NOT NULL');
        $this->addSql('ALTER TABLE facture DROP FOREIGN KEY FK_FE866410F675F31B');
        $this->addSql('DROP INDEX IDX_FE866410F675F31B ON facture');
        $this->addSql('ALTER TABLE facture DROP author_id');
        $this->addSql('ALTER TABLE forfait DROP FOREIGN KEY FK_BBB5C482F675F31B');
        $this->addSql('DROP INDEX IDX_BBB5C482F675F31B ON forfait');
        $this->addSql('ALTER TABLE forfait DROP author_id');
    }
}
