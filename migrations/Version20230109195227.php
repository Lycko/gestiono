<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230109195227 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE collaborateur_forfait (collaborateur_id INT NOT NULL, forfait_id INT NOT NULL, INDEX IDX_9C8A8D70A848E3B1 (collaborateur_id), INDEX IDX_9C8A8D70906D5F2C (forfait_id), PRIMARY KEY(collaborateur_id, forfait_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE collaborateur_forfait ADD CONSTRAINT FK_9C8A8D70A848E3B1 FOREIGN KEY (collaborateur_id) REFERENCES collaborateur (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE collaborateur_forfait ADD CONSTRAINT FK_9C8A8D70906D5F2C FOREIGN KEY (forfait_id) REFERENCES forfait (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE facture ADD collaborateurs_fact_id INT NOT NULL');
        $this->addSql('ALTER TABLE facture ADD CONSTRAINT FK_FE86641056746099 FOREIGN KEY (collaborateurs_fact_id) REFERENCES collaborateur (id)');
        $this->addSql('CREATE INDEX IDX_FE86641056746099 ON facture (collaborateurs_fact_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE collaborateur_forfait DROP FOREIGN KEY FK_9C8A8D70A848E3B1');
        $this->addSql('ALTER TABLE collaborateur_forfait DROP FOREIGN KEY FK_9C8A8D70906D5F2C');
        $this->addSql('DROP TABLE collaborateur_forfait');
        $this->addSql('ALTER TABLE facture DROP FOREIGN KEY FK_FE86641056746099');
        $this->addSql('DROP INDEX IDX_FE86641056746099 ON facture');
        $this->addSql('ALTER TABLE facture DROP collaborateurs_fact_id');
    }
}
