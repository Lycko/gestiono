<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230102190943 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE forfait (id INT AUTO_INCREMENT NOT NULL, nom_formule VARCHAR(255) DEFAULT NULL, num_formule VARCHAR(255) DEFAULT NULL, description LONGTEXT NOT NULL, quantite INT DEFAULT NULL, montant_q DOUBLE PRECISION DEFAULT NULL, nombre_h DOUBLE PRECISION DEFAULT NULL, montant_h DOUBLE PRECISION DEFAULT NULL, tva DOUBLE PRECISION DEFAULT 0, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE forfait');
    }
}
