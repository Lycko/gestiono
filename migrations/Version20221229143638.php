<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221229143638 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user ADD entreprise VARCHAR(255) NOT NULL, ADD nom VARCHAR(255) NOT NULL, ADD prenom VARCHAR(255) NOT NULL, ADD telephone VARCHAR(255) DEFAULT NULL, ADD date_naissance DATE NOT NULL, ADD siret VARCHAR(255) DEFAULT NULL, ADD siren VARCHAR(255) DEFAULT NULL, ADD adresse_user VARCHAR(255) DEFAULT NULL, ADD adresse_entreprise VARCHAR(255) DEFAULT NULL, ADD code_postal VARCHAR(255) DEFAULT NULL, ADD ville VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE `user` DROP entreprise, DROP nom, DROP prenom, DROP telephone, DROP date_naissance, DROP siret, DROP siren, DROP adresse_user, DROP adresse_entreprise, DROP code_postal, DROP ville');
    }
}
